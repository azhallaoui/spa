import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { User } from './user';

@Injectable()
export class UserService {
    // Url to API
    private userUrl = 'https://jsonplaceholder.typicode.com/users';

    // Injecting the http client into the service
    constructor(private http: Http) {}

    getUsers (): Observable<User []> {
        return this.http.get(this.userUrl)
            .map(this.parseData)
            .catch(this.handleError);
    }

    addUsers (user: User ): Observable<User> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});

        return this.http.post(this.userUrl, { user }, options)
            .map(this.parseData)
            .catch(this.handleError);
    }

    private parseData(res: Response)  {
        let body = res.json();

        if (body instanceof Array) {
            return body || [];
        }

        else return body.user || {};
    }

    deleteUsers(userId): Observable<User []> {
        return this.http.delete(this.userUrl + '/' + userId)
            .map(this.parseData)
            .catch(this.handleError);
    }


updateUsers(user: User): Observable<User> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers});
    return this.http.put(this.userUrl + '/' + user.id, { user }, options)
        .map(this.parseData)
        .catch(this.handleError);
}
    // Prases error based on the format
    private handleError(error: Response | any) {
        let errorMessage: string;

        errorMessage = error.message ? error.message : error.toString();

        // In real world application, call to log error to remote server
        // logError(error);
        return Observable.throw(errorMessage);
    }
}
