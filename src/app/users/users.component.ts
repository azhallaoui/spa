import { Component, OnInit } from '@angular/core';
import { UserService } from './users.service';
import { User } from './user';
@Component({
  selector: 'users',
  providers: [UserService],
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  selectedUser: User;
  errorMessage: string;
  users: User[];
  editForm = false;
  // We don't call the get method in the constructor
  constructor(private userService: UserService) { }
  // Fetching the records in the onInit lifecycle method makes the application easier to debug
  ngOnInit() { this.getUsers(); }
  edit(user: User): void {
    this.editForm = true
    this.selectedUser = user;
  }
  getUsers() {
      this.userService.getUsers().subscribe(
          users => this.users = users,
          error => this.errorMessage = <any>error
      )
  }
  createUser(user: User) {
      if (!user.id) { return; }
      this.userService.addUsers(user).subscribe(
          newUser => this.users =  [...this.users, newUser],
          error => this.errorMessage = <any>error
      )
  }

  deleteUser(userId): void {
    this.users = this.users.filter(u => u.id !== userId);
    this.userService.deleteUsers(userId).subscribe();
  }

  updateUser(user: User) {
      var actualUser = this.users.find(u => u.id == user.id);
      var ind = this.users.indexOf(actualUser);
    this.userService.updateUsers(user).subscribe(
        editedUserd => this.users[ind]=user
          )

  }

  onRowClick(event, id){
    console.log(event.target.outerText, id);
  }
}
